import logo from './logo.svg';
import './App.css';
import cheems1 from './assets/cheems1.jpeg';
import cheems2 from './assets/cheems2.jpeg';
import cheems3 from './assets/cheems3.jpg';
import cheems4 from './assets/cheems4.jpeg';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <div>
            
          <img src={cheems1} className="App-logo" alt="cheems1" />
          <img src={cheems2} className="App-logo" alt="cheems2" />
          <img src={cheems3} className="App-logo" alt="cheems3" />
          <img src={cheems4} className="App-logo" alt="cheems4" />
        </div>
      </header>
    </div>
  );
}

export default App;
