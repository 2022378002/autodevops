let self = this;

const CACHE_DYNAMIC = 'dynamic-v1';
const CACHE_STATIC = 'static-v1';
const CACHE_INMUTABLE = 'inmutable-v1'

const limpiarCache = (cacheName, numeroItems) => {
    caches.open(cacheName)
        .then(cache => {
            return cache.keys()
                .then(keys => {
                    if (keys.length > numeroItems) {
                        cache.delete(keys[0])
                            .then(limpiarCache(cacheName, numeroItems))
                    }
                })
        })
}

self.addEventListener('install', function (event) {

    const cachePromise = caches.open(CACHE_STATIC).then(function (cache) {
        return cache.addAll([
            '/',
            '/index.html',
            '/js/app.js',
            '/sw.js',
            '/static/js/bundle.js',
            'favicon.ico'
        ])
    })

    const cacheInmutable = caches.open(CACHE_INMUTABLE).then(function (cache) {
        return cache.addAll([
            'https://fonts.googleapis.com/css2?family=Comfortaa:wght@400;500;700&display=swap" rel="stylesheet'
        ])
    })
    event.waitUntil(Promise.all([cachePromise, cacheInmutable]))

})

// se busca el recurso en la caché primero utilizando caches.match(event.request). Si el recurso está en la caché, se devuelve la versión en caché. Si no está en la caché, se busca en la red utilizando fetch(event.request). Si se encuentra en la red, se almacena en la caché CACHE_DYNAMIC y se utiliza una función limpiarCache para asegurarse de que la caché no crezca demasiado.
// self.addEventListener('fetch', function (event) {
//     // Cache network fllback
//     const respuesta = caches.match(event.request)
//         .then(response => {
//             if (response) return response
//             //Si no existe
//             return fetch(event.request)
//                 .then(newResponse => {

//                     caches.open(CACHE_DYNAMIC)
//                         .then(cache => {
//                             cache.put(event.request, newResponse)
//                             limpiarCache(CACHE_DYNAMIC, 2)
//                         }
//                         )
//                     return newResponse.clone()
//                 }
//                 )
//         })
//     event.respondWith(respuesta)
// })


// // Esta metodo primero intenta obtener el recurso desde Internet y, si eso falla, la busca en la caché.
// self.addEventListener('fetch', function (event) {
//     // Intenta obtener el recurso de Internet primero
//     event.respondWith(
//         fetch(event.request)
//             .then(networkResponse => {
//                 // Si se obtiene una respuesta de la red, la almacenamos en la caché dinámica
//                 caches.open(CACHE_DYNAMIC)
//                     .then(cache => {
//                         cache.put(event.request, networkResponse);
//                         limpiarCache(CACHE_DYNAMIC, 4);
//                     });
//                 // Devolvemos la respuesta de la red
//                 return networkResponse.clone();
//             })
//             .catch(() => {
//                 // Si falla la solicitud a Internet, intenta obtener el recurso de la caché
//                 return caches.match(event.request)
//                     .then(cachedResponse => {
//                         if (cachedResponse) {
//                             // Si el recurso está en la caché, lo devolvemos
//                             return cachedResponse;
//                         } else {
//                             // Si no está en la caché, devolvemos una respuesta de fallback
//                             // o una página de error personalizada
//                             return new Response('No hay conexión a Internet');
//                         }
//                     });
//             })
//     );
// });

// Esta metodo primero intenta obtener el recurso desde Internet y, si eso falla, la busca en la caché.
self.addEventListener('fetch', function (event) {
    // Intenta obtener el recurso de Internet primero
    event.respondWith(
        fetch(event.request)
            .then(networkResponse => {
                // Si se obtiene una respuesta de la red, la almacenamos en la caché dinámica
                caches.open(CACHE_DYNAMIC)
                    .then(cache => {
                        cache.put(event.request, networkResponse);
                        limpiarCache(CACHE_DYNAMIC, 4);
                    });
                // Devolvemos la respuesta de la red
                return networkResponse.clone();
            })
            .catch(() => {
                // Si falla la solicitud a Internet, intenta obtener el recurso de la caché
                return caches.match(event.request)
                    .then(cachedResponse => {
                        if (cachedResponse) {
                            // Si el recurso está en la caché, lo devolvemos
                            return cachedResponse;
                        } else {
                            // Si no está en la caché, devolvemos una respuesta de fallback
                            // o una página de error personalizada
                            return new Response('No hay conexión a Internet');
                        }
                    });
            })
    );
});