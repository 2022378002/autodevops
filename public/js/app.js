
// REGISTRO DEL SW EN EL APP.JS 
// if(navigator.serviceWorker){
//     navigator.serviceWorker.register('/sw.js')
  
// }
const registerServiceWorker = async () => {
    if ("serviceWorker" in navigator) {
        try {
            const registration = await navigator.serviceWorker.register("/sw.js", {
                scope: "/",
            });
            if (registration.installing) {
                console.log("Service worker installing");
            } else if (registration.waiting) {
                console.log("Service worker installed");
            } else if (registration.active) {
                console.log("Service worker active");
            }
        } catch (error) {
            console.error(`Registration failed with ${error}`);
        }
    }
};

registerServiceWorker();


// if (window.caches) {
//     caches.open("cache-v1.1").then((cache) => {
//         cache
//             .addAll([
//                 "/",
//                 "/index.html",
//                 "/css/style.css",
//                 "/img/main.jpg",
//             ])
//             .then(() => {
//                 console.log("Resources cached");
//             })
//             .catch((error) => {
//                 console.error("Error caching resources:", error);
//             });

//         cache
//             .match("index.html")
//             .then((response) => {
//                 if (response) {
//                     return response.text();
//                 }
//                 throw new Error("No response found in cache");
//             })
//             .then((html) => {
//                 console.log("Content of index.html:", html);
//             })
//             .catch((error) => {
//                 console.error("Error getting index.html from cache:", error);
//             });
//     });
// }